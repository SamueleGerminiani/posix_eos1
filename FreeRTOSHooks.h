#ifndef FREERTOSHOOKS_H_
#define FREERTOSHOOKS_H_

#include "FreeRTOS.h"
#include "task.h"
extern int counter;
void vApplicationMallocFailedHook(void);
void vApplicationStackOverflowHook(xTaskHandle pxTask, signed char *pcTaskName);
void vApplicationIdleHook(void);

#endif
