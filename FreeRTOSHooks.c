#include "FreeRTOSHooks.h"

void vApplicationMallocFailedHook(void) {
  for (;;)
    ;
}

void vApplicationStackOverflowHook(xTaskHandle pxTask,
                                   signed char *pcTaskName) {
  (void)pcTaskName;
  (void)pxTask;

  for (;;)
    ;
}

void vApplicationIdleHook(void) { }
